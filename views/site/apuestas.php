<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Ciclista;

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de Apuestas</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Bootstrap JS y Popper.js (si estás utilizando la versión 4.x) -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <!-- Select2 JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <style>
        body {
            margin: 0;
            display: flex;
        }

        #sidebar {
            width: 250px;
            height: 100vh;
            background-color: #ccc;
            position: fixed;
            top: 20px;
            left: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 20px;
        }

        table {
            border-collapse: collapse;
            width: 70%;
            margin-left: 250px;
        }

        th, td {
            border: 1px solid #000;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #000;
            color: #fff;
        }

        #apuesta-form, #confirmacion-form {
            display: flex;
            flex-direction: column;
            width: 100%;
            margin-top: 20px;
        }

        #confirmacion-form {
            display: none;
        }

        input, select, button {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
   <div class="row">
    <div class="col-sm-6">
        <label for="ciclista">Ciclista:</label>
        <?php
        echo Select2::widget([
            'name' => 'ciclista_id',
            'data' => \yii\helpers\ArrayHelper::map(
                \app\models\Ciclista::find()->all(),
                'dorsal',
                'nombre'
            ),
            'size' => Select2::MEDIUM,
            'addon' => [
                'prepend' => [
                    'content' => '<i class="fas fa-user"></i>'
                ],
                'append' => [
                    'content' => Html::button('<i class="fas fa-info-circle"></i>', [
                        'class' => 'btn btn-primary',
                        'title' => 'Información',
                        'data-toggle' => 'tooltip'
                    ]),
                    'asButton' => true
                ]
            ],
            'options' => ['placeholder' => 'Selecciona un ciclista ...', 'autocomplete' => 'off'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
        <label for="tu-apuesta">Tu apuesta:</label>
        <input type="number" id="tu-apuesta" name="tu-apuesta" placeholder="Ingrese su apuesta" oninput="calcularGanancia()">

        <label for="categoria">Elige categoría:</label>
        <select id="categoria" name="categoria">
            <option value="etapas">Etapas</option>
            <option value="puertos">Puertos</option>
        </select>

        <label for="multiplicador">Multiplicador:</label>
        <span id="multiplicador">0.000</span>

        <label for="tu-ganancia">Tu ganancia:</label>
        <span id="tu-ganancia">0.000</span>

        <button type="button" onclick="confirmarApuesta()">Aceptar</button>
    </div>

    <form id="confirmacion-form">
        <h2>Introduce tu correo electrónico para verificar la apuesta</h2>
        <label for="correo">Correo electrónico:</label>
        <input type="text" id="correo" name="correo" placeholder="example@tucorreo.com">
        <button type="button" onclick="apuestaConfirmada()">Aceptar</button>
    </form>
</div>

<table>
    <thead>
        <tr>
            <th>Ciclista</th>
            <th>Multiplicador para Etapas</th>
            <th>Multiplicador para Puertos</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($resultados as $resultado): ?>
            <tr>
                <td><?= $resultado['nombre'] ?></td>
                <td><?= number_format($multiplicadores[$resultado['nombre']]['etapas'], 3) ?></td>
                <td><?= number_format($multiplicadores[$resultado['nombre']]['puertos'], 3) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script>
    function confirmarApuesta() {
        document.getElementById('apuesta-form').style.display = 'none';
        document.getElementById('confirmacion-form').style.display = 'flex';
    }

    function apuestaConfirmada() {
        alert("Apuesta realizada. Confirma en tu correo electrónico.");
        document.getElementById('apuesta-form').style.display = 'flex';
        document.getElementById('confirmacion-form').style.display = 'none';
    }

    function calcularGanancia() {
        const tuApuesta = parseFloat(document.getElementById('tu-apuesta').value) || 0;
        const multiplicador = parseFloat(document.getElementById('multiplicador').innerText) || 0;
        const tuGanancia = tuApuesta * multiplicador;

        document.getElementById('tu-ganancia').innerText = tuGanancia.toFixed(3);
    }
</script>
</body>
</html>
