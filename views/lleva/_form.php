<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\lleva $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="lleva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dorsal')->textInput() ?>

    <?= $form->field($model, 'numetapa')->textInput() ?>

    <?= $form->field($model, 'código')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
