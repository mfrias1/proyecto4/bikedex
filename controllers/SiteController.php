<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    /* Para que se renderice la vista y no haga falta hacer un controller */
    public function actionNoticias(){
        return $this->render('noticias');
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
   public function actionListarCiclistas()
{
    $ciclistas = Ciclista::find()
        ->select(['nombre'])
        ->groupBy(['nombre'])
        ->asArray()
        ->all();
    return $this->render('listarCiclistas', ['ciclistas' => $ciclistas]);
}
    
     public function actionApuestas()
    {
      
        $resultados = Ciclista::find()
            ->select([
                'c.nombre',
                'COUNT(DISTINCT e.numetapa) as total_etapas', // Utiliza COUNT(DISTINCT) para evitar contar etapas duplicadas
                'COUNT(DISTINCT p.Nompuerto) as total_puertos', // Contar puertos distintos
            ])
            ->from(['c' => 'ciclista'])
            ->leftJoin('lleva l', 'c.dorsal = l.dorsal')
            ->leftJoin('etapa e', 'l.numetapa = e.numetapa')
            ->leftJoin('puerto p', 'e.dorsal = p.dorsal')
            ->groupBy(['c.nombre'])
            ->orderBy(['total_etapas' => SORT_DESC]) // Ordenar por total_etapas de mayor a menor
            ->asArray()
            ->all();

        // Simula los multiplicadores sin afectar la base de datos
        $baseMultiplicadorEtapas = 2;
        $baseMultiplicadorPuertos = 3; // Puedes ajustar este valor según tus necesidades

        $multiplicadores = [];

        foreach ($resultados as $resultado) {
            $totalEtapas = $resultado['total_etapas'];
            $totalPuertos = $resultado['total_puertos'];

            // Ajusta cómo calculas los multiplicadores en base a los totales de etapas y puertos
            $multiplicadores[$resultado['nombre']] = [
                'etapas' => $baseMultiplicadorEtapas / max(1, $totalEtapas),
                'puertos' => $baseMultiplicadorPuertos / max(1, $totalPuertos),
            ];
        }

        return $this->render('apuestas', [
            'resultados' => $resultados,
            'multiplicadores' => $multiplicadores,
        ]);
    }
    
    
    
    
    
    
  
}
    
